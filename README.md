Пример тестового задания
---------------------

Основной репозиторий с кодом проекта [symfony-react-sandbox](https://github.com/Limenius/symfony-react-sandbox).

#### Локальная сборка images:

```no-highlight
cd nginx
docker build -t enotter/ms/nginx:latest .
```

```no-highlight
cd app
docker build -t enotter/ms/app:latest .
```

```no-highlight
cd mysql
docker build -t enotter/ms/mysql:latest .
```

#### Работа с ansible

Сборка images локально:

```no-highlight
ansible-playbook -i ansible/inventory ansible/playbook/docker-build.yml
```

Деплой контейнеров:

```no-highlight
ansible-playbook -i ansible/inventory ansible/playbook/docker-deploy.yml
```

#### Локальный запуск проекта

docker-compose up
