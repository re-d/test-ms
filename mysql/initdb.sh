#!/bin/bash

/usr/bin/mysqld_safe --skip-grant-tables &
sleep 10
#mysql -u root -e "CREATE DATABASE sfreact"
mysql -u root -e "CREATE USER 'sfreact'@'localhost' IDENTIFIED BY 'sfreact123'"
mysql -u root -e "ALTER USER 'sfreact'@'localhost' IDENTIFIED WITH mysql_native_password BY 'sfreact123'"
mysql -u root -e "GRANT ALL PRIVILEGES ON recipe. * TO 'sfreact'@'localhost'"
mysql -u root -e "FLUSH PRIVILEGES"
mysql -u root sfreact < /tmp/sfreact.sql
