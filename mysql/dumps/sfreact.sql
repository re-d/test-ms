-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: sfreact
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `recipe`
--

DROP TABLE IF EXISTS `recipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipe`
--

LOCK TABLES `recipe` WRITE;
/*!40000 ALTER TABLE `recipe` DISABLE KEYS */;
INSERT INTO `recipe` VALUES (1,'Paella','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consequat efficitur venenatis. Proin eros nisl, maximus id tristique et, placerat a felis. Integer finibus lacinia pretium. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla ipsum libero, fringilla id mollis molestie, maximus quis lectus. Sed euismod, risus interdum mattis rhoncus, lectus sem lacinia eros, eu gravida sapien nunc eu risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras ultrices nunc eget congue viverra. Nulla finibus metus mi, in porttitor neque laoreet quis. Proin ut lacinia felis, sit amet mollis ligula. In tincidunt posuere volutpat. Morbi aliquet nunc dolor, ut consequat lacus maximus vitae. Donec orci sem, interdum quis scelerisque et, tincidunt ut nibh. Vivamus bibendum suscipit urna, et rutrum felis suscipit non.','paella.jpg'),(2,'Carbonara','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consequat efficitur venenatis. Proin eros nisl, maximus id tristique et, placerat a felis. Integer finibus lacinia pretium. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla ipsum libero, fringilla id mollis molestie, maximus quis lectus. Sed euismod, risus interdum mattis rhoncus, lectus sem lacinia eros, eu gravida sapien nunc eu risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras ultrices nunc eget congue viverra. Nulla finibus metus mi, in porttitor neque laoreet quis. Proin ut lacinia felis, sit amet mollis ligula. In tincidunt posuere volutpat. Morbi aliquet nunc dolor, ut consequat lacus maximus vitae. Donec orci sem, interdum quis scelerisque et, tincidunt ut nibh. Vivamus bibendum suscipit urna, et rutrum felis suscipit non.','carbonara.jpg'),(3,'Falafel','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consequat efficitur venenatis. Proin eros nisl, maximus id tristique et, placerat a felis. Integer finibus lacinia pretium. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla ipsum libero, fringilla id mollis molestie, maximus quis lectus. Sed euismod, risus interdum mattis rhoncus, lectus sem lacinia eros, eu gravida sapien nunc eu risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras ultrices nunc eget congue viverra. Nulla finibus metus mi, in porttitor neque laoreet quis. Proin ut lacinia felis, sit amet mollis ligula. In tincidunt posuere volutpat. Morbi aliquet nunc dolor, ut consequat lacus maximus vitae. Donec orci sem, interdum quis scelerisque et, tincidunt ut nibh. Vivamus bibendum suscipit urna, et rutrum felis suscipit non.','falafel.jpg'),(4,'Sushi','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consequat efficitur venenatis. Proin eros nisl, maximus id tristique et, placerat a felis. Integer finibus lacinia pretium. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla ipsum libero, fringilla id mollis molestie, maximus quis lectus. Sed euismod, risus interdum mattis rhoncus, lectus sem lacinia eros, eu gravida sapien nunc eu risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras ultrices nunc eget congue viverra. Nulla finibus metus mi, in porttitor neque laoreet quis. Proin ut lacinia felis, sit amet mollis ligula. In tincidunt posuere volutpat. Morbi aliquet nunc dolor, ut consequat lacus maximus vitae. Donec orci sem, interdum quis scelerisque et, tincidunt ut nibh. Vivamus bibendum suscipit urna, et rutrum felis suscipit non.','sushi.jpg');
/*!40000 ALTER TABLE `recipe` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-30 12:38:12
